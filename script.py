#! /usr/bin/env python
#
import gpiozero as g
import time as t
from asyncio_mqtt import Client
import asyncio

MQTT_HOST = "new.hackens.org"
TOPIC = "hackens/trash"

output = "GPIO15"
inpt = "GPIO14"

outPort = g.DigitalOutputDevice(output)
inPort = g.DigitalInputDevice(inpt)

def get_raw_data():
    inPort.wait_for_inactive()
    outPort.on()
    t.sleep(0.001)
    outPort.off()
    inPort.wait_for_active(timeout=1)
    pTime = t.time()
    inPort.wait_for_inactive(timeout=1)
    pulseLength = t.time() - pTime
    return pulseLength

async def main():
    async with Client(MQTT_HOST) as client:
        while True:
            await client.publish(TOPIC, payload = get_raw_data(), retain=True)
            await asyncio.sleep(1);

asyncio.run(main())
